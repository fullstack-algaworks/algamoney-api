CREATE TABLE pessoa (
  id BIGINT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  ativo BOOLEAN NOT NULL,
  logradouro VARCHAR(255),
  numero BIGINT,
  complemento TEXT,
  bairro VARCHAR(50),
  cep VARCHAR(10),
  cidade VARCHAR(255),
  estado VARCHAR(255),
  PRIMARY KEY (id)
);
