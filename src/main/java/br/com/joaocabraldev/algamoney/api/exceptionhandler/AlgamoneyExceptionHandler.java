package br.com.joaocabraldev.algamoney.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.joaocabraldev.algamoney.api.exceptionhandler.Problema.Erro;

@ControllerAdvice
public class AlgamoneyExceptionHandler extends ResponseEntityExceptionHandler {
    
    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        
        List<Problema.Erro> erros = new ArrayList<>();
        Erro erro = Erro.builder()
            .mensagemUsuario("Ocorreu um erro na validação dos campos passados.")
            .build();
        erros.add(erro);

        Problema problema = Problema.builder()
                .dataHora(OffsetDateTime.now())
                .titulo("Erro de validação de campos")
                .erros(erros)
                .status(HttpStatus.BAD_REQUEST.value())
                .build();
        
        List<Problema> problemas = Arrays.asList(problema);

        return handleExceptionInternal(ex, problemas, headers, status, request);

    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        Problema problema = Problema.builder()
                .dataHora(OffsetDateTime.now())
                .titulo("Recurso não encontrado")
                .status(HttpStatus.NOT_FOUND.value())
                .build();
        
        List<Problema> problemas = Arrays.asList(problema);
        return handleExceptionInternal(ex, problemas, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        
        List<Problema> problemas = criarListaProblemas(ex.getBindingResult(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, problemas, headers, HttpStatus.BAD_REQUEST, request);

    }

    private List<Problema> criarListaProblemas(BindingResult bindingResult, HttpStatus status) {
        List<Problema> problemas = new ArrayList<>();
        List<Problema.Erro> erros = new ArrayList<>();

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            String mensagem = messageSource.getMessage(fieldError, 
                LocaleContextHolder.getLocale());
            
            Erro campo = Erro.builder()
                .mensagemUsuario(mensagem)
                .build();

            erros.add(campo);
            
        }

        Problema problema = Problema.builder()
            .status(status.value())
            .titulo("Preenchimento incorreto dos campos")
            .dataHora(OffsetDateTime.now())
            .erros(erros)
            .build();
        problemas.add(problema);

        return problemas;
    }

}
