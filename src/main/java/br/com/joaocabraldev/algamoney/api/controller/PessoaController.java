package br.com.joaocabraldev.algamoney.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabraldev.algamoney.api.model.Pessoa;
import br.com.joaocabraldev.algamoney.api.repository.PessoaRepository;
import br.com.joaocabraldev.algamoney.api.service.PessoaService;
import javassist.NotFoundException;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {
    
    private PessoaRepository pessoaRepository;
    private PessoaService pessoaService;

    @Autowired
    public PessoaController(PessoaRepository pessoaRepository, 
        PessoaService pessoaService) {
        this.pessoaRepository = pessoaRepository;
        this.pessoaService = pessoaService;
    }

    @GetMapping
    public List<Pessoa> listar() {
        return pessoaRepository.findAll();
    }

    @GetMapping("/{codigoPessoa}")
    public ResponseEntity<Pessoa> buscarPorCodigo(@PathVariable Long codigoPessoa) 
        throws NotFoundException {
        return pessoaRepository.findById(codigoPessoa)
            .map(pessoa -> ResponseEntity.ok(pessoa))
            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pessoa criar(@Valid @RequestBody Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @PutMapping("/{codigoPessoa}")
    public Pessoa atualizar(@PathVariable Long codigoPessoa, 
        @Valid @RequestBody Pessoa pessoa) {
        return pessoaService.atualizar(codigoPessoa, pessoa);
    }

    @DeleteMapping("/{codigoPessoa}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Long codigoPessoa) {
        pessoaRepository.deleteById(codigoPessoa);
    }

}
