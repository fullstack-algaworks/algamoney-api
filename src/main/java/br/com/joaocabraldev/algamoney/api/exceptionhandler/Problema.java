package br.com.joaocabraldev.algamoney.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class Problema {
    
    private Integer status;
	private OffsetDateTime dataHora;
	private String titulo;
	private List<Erro> erros;

    @Getter
	@Builder
	public static class Erro {
		
		private String mensagemUsuario;
		
	}

}
