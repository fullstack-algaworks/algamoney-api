package br.com.joaocabraldev.algamoney.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabraldev.algamoney.api.model.Pessoa;
import br.com.joaocabraldev.algamoney.api.repository.PessoaRepository;

@Service
public class PessoaService {
    
    private PessoaRepository pessoaRepository;

    @Autowired
    public PessoaService(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    public Pessoa atualizar(Long codigoPessoa, Pessoa pessoa) {
        Pessoa pessoaSalva = pessoaRepository.findById(codigoPessoa)
            .orElseThrow(() -> new EmptyResultDataAccessException(1));;
        
        BeanUtils.copyProperties(pessoa, pessoaSalva, "id");
        
        return pessoaRepository.save(pessoaSalva);
    }

}
