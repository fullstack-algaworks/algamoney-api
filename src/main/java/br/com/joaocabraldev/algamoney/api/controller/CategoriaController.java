package br.com.joaocabraldev.algamoney.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabraldev.algamoney.api.model.Categoria;
import br.com.joaocabraldev.algamoney.api.repository.CategoriaRepository;
import javassist.NotFoundException;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {
    
    private CategoriaRepository categoriaRepository;

    @Autowired
    public CategoriaController(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    @GetMapping
    public List<Categoria> listar() {
        return categoriaRepository.findAll();
    }

    @GetMapping("/{codigoCategoria}")
    public ResponseEntity<Categoria> buscarPorCodigo(@PathVariable Long codigoCategoria)
        throws NotFoundException {
        return categoriaRepository.findById(codigoCategoria)
            .map(categoria -> ResponseEntity.ok(categoria))
            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categoria criar(@Valid @RequestBody Categoria categoria) {
        return categoriaRepository.save(categoria);
    }

}
