package br.com.joaocabraldev.algamoney.api.it;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import br.com.joaocabraldev.algamoney.api.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoriasIT {
    
    @LocalServerPort
	private int port;

	private int quantidadeItensCadastrados;
	private String jsonCorreto;

    @BeforeAll
    public void setup() {

		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/categorias";

		jsonCorreto = ResourceUtils.getContentFromResource(
				"/json/categorias.json");
    }

    @Test
	public void deveRetornarStatus200EDadosCorretosConsultarCategoria() {
		given()
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
            .statusCode(HttpStatus.OK.value())
            .body("", equalTo(jsonCorreto));
    }
    
    @Test
	public void deveRetornarQuantidadeCorretaConsultarCategorias() {
		given()
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeItensCadastrados));
	}

}
